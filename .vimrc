syntax on
filetype plugin on

" Leader key
let mapleader="\\"
let maplocalleader="\\"

" File encoding
set encoding=utf-8

" Line numbering
set number relativenumber
set numberwidth=4

" Tabulation
set shiftwidth=4
set tabstop=4
set noexpandtab
set smarttab
set autoindent

" Wrapping
set wrap

" Representation
set listchars=eol:¬,tab:>-,space:·
set list

" Color scheme
colorscheme zellner

" Spelling
set spell spelllang=en_us

" Recursive file
set path+=**

" Display search menu
set wildmenu

" Generate Tags
command! GenTags !ctags -R .

" File Browsing {{{

" Tweaks for browsing
let g:netrw_banner=0        " disable annoying banner
let g:netrw_browse_split=4  " open in prior window
let g:netrw_altv=1          " open splits to the right
let g:netrw_liststyle=3     " tree view
let g:netrw_list_hide=netrw_gitignore#Hide()
let g:netrw_list_hide.=',\(^\|\s\s\)\zs\.\S\+'
" }}}

" Configure the `make` command to run RSpec
set makeprg=bundle\ exec\ rspec\ -f\ QuickfixFormatter

" Mappings {{{

nnoremap <localleader>fn :echo expand("%")<cr>

" Disabling some bad keys {{{
nnoremap <Up> <nop>
nnoremap <Down> <nop>
nnoremap <Left> <nop>
nnoremap <Right> <nop>
inoremap <Up> <nop>
inoremap <Down> <nop>
inoremap <Left> <nop>
inoremap <Right> <nop>
vnoremap <Up> <nop>
vnoremap <Down> <nop>
vnoremap <Left> <nop>
vnoremap <Right> <nop>
" }}}

" Handle VIMRC {{{
nnoremap <localleader>rc :vsplit $MYVIMRC<cr>
nnoremap <localleader>so :source $MYVIMRC<cr>:echom "source from" $MYVIMRC<cr>
" }}}


" Surrounding text {{{
vnoremap <localleader>" <esc>`>a"<esc>`<i"<esc>
vnoremap <localleader>' <esc>`>a'<esc>`<i'<esc>
vnoremap <localleader>` <esc>`>a`<esc>`<i`<esc>
vnoremap <localleader>( <esc>`>a)<esc>`<i(<esc>
vnoremap <localleader>{ <esc>`>a}<esc>`<i{<esc>
vnoremap <localleader>[ <esc>`>a]<esc>`<i[<esc>
" }}}

inoremap <localleader>ctime <C-R>=strftime("%Y-%m-%d %a %I:%M %p")<CR>

" FileType mappings {{{
augroup clanggroup
	autocmd!
	autocmd FileType c inoremap <buffer> <localleader>if if () {<enter>}<esc>kf)i
	autocmd FileType c inoremap <buffer> <localleader>for for (;;) {<enter>}<esc>kf;i
	autocmd FileType c inoremap <buffer> <localleader>while while () {<enter>}<esc>kf)i
	autocmd FileType c inoremap <buffer> <localleader>dowhile do {<enter>} while ();<esc>i
	autocmd FileType c inoremap <buffer> <localleader>switch switch () {<enter>}<esc>O<tab>default:<enter><tab>break;<esc>2kT(i
	autocmd FileType c inoremap <buffer> <localleader>{ type name (param)<enter>{<enter>}<esc>2k
	autocmd FileType c inoremap <buffer> <localleader>s struct {<enter>};<esc>kf{
	autocmd FileType c inoremap <buffer> <localleader>u union {<enter>};<esc>kf{
	autocmd FileType c inoremap <buffer> <localleader>d #define 
	autocmd FileType c inoremap <buffer> <localleader>< #include <.h><esc>2hi
	autocmd FileType c inoremap <buffer> <localleader>" #include ".h"<esc>2hi
	autocmd FileType c inoremap <buffer> <localleader>mip #ifndef <enter>#endif<esc>O<tab>#define <esc>kA
	autocmd FileType c inoremap <buffer> <localleader>main int main (int argc, char ** argv)<enter>{<enter>}<esc>O
	autocmd FileType c inoremap <buffer> <localleader>pr printf("", arg);<esc>2T"i
	autocmd FileType c inoremap <buffer> <localleader>sc scanf("", arg);<esc>2T"i
	" comments {{{
	"
	autocmd FileType c inoremap <buffer> <localleader>fdoc /**<enter>\file filename<enter>\brief desc<enter>\author sangsoic <sangsoic@protonmail.com><enter>\version 0.1<enter>\date <C-R>=strftime("%Y-%m-%d %a %I:%M %p")<CR><enter>/<esc>6kw
	autocmd FileType c inoremap <buffer> <localleader>ffdoc /**<enter>\fn header<enter>\brief short_desc<enter>\author sangsoic <sangsoic@protonmail.com><enter>\version 0.1<enter>\date <C-R>=strftime("%Y-%m-%d %a %I:%M %p")<CR><enter>\param param desc<enter>\return value desc<enter>/<esc>8k3w

	" }}}
augroup END

augroup htmlgroup
	autocmd!
	autocmd FileType html setlocal shiftwidth=2
	autocmd FileType html setlocal tabstop=2
	autocmd FileType html setlocal expandtab
	autocmd FileType html inoremap <buffer> <localleader>root <esc>:setlocal noautoindent<cr>i<!DOCTYPE html><cr><html lang="en-us"><cr><head><cr><tab><meta charset="utf-8" /><cr><tab><meta name="viewport" content="width=device-width, initial-scale=1.0" /><cr><tab><title>NeverOutshineTheMaster</title><cr></head><cr><body><cr><cr></body><cr></html><esc>:setlocal autoindent<cr>
	autocmd FileType html inoremap <buffer> <localleader><! <!----><esc>2hi
	autocmd FileType html inoremap <buffer> <localleader><b <><cr>content<cr></tab><esc>4ba
	autocmd FileType html inoremap <buffer> <localleader><i <>content</tab><esc>4ba
	autocmd FileType html inoremap <buffer> <localleader><e < /><esc>2ba
augroup END

augroup cssgroup
	autocmd!
	autocmd FileType css setlocal shiftwidth=2
	autocmd FileType css setlocal tabstop=2
	autocmd FileType css setlocal expandtab
	autocmd FileType css inoremap <buffer> <localleader>/* /**/<esc>b2li
	autocmd FileType css inoremap <buffer> <localleader>s  {<cr>}<esc>ki
augroup END

" }}}

" }}}
