#!/bin/bash

# Prints a loading bar at a specific progress state.
print_loading_bar ()
{
	local status progress width progressWidth sharps
	status=1
	width="$2"

	# Tests number of parameters
	if (( $# == 2 )); then
		# Validates progress parameter
		if (( `bc <<< "$1 <= 1" 2>/dev/null` )); then
			progress="$1"

			progressWidth=$(printf '%.0f' $(bc -l <<< "${progress}*${width}"))
			echo -n [

			#Prints sharps
			printf -v sharps '%*s' $progressWidth
			sharps=${sharps// /\#}
			echo -n $sharps

			#Prints trailing spaces
			printf '%*s' $(( $width - $progressWidth ))

			echo -n ]

			status=0
		else
			echo error: subtlehash: print_loading_bar: invalid progress parameter 1>&2
		fi
	else
		echo error: subtlehash: print_loading_bar: need precisely 2 parameters 1>&2
	fi

	return $status
}

# Loads wait bar of a given width for a given delay.
load_wait_bar ()
{
	local status delay width sec
	status=1

	# Tests number of parameters
	if (( $# == 2 )); then
		# Validates delay parameter
		if [[ "$1" =~ ^[1-9][0-9]*$ ]]; then
			delay="$1"
			# Validates width parameter
			if (( "$2" > 5 )) 2>/dev/null ; then
				width="$2"
				sec=0

				#Refreshes loading bar every second
				print_loading_bar $sec/$delay $width
				printf " $sec/$delay s\r"
				for (( sec=1 ; sec < $delay ; sec++)); do
					sleep 1
					print_loading_bar $sec/$delay $width
					printf " $sec/$delay s\r"
				done
				sleep 1
				print_loading_bar $sec/$delay $width
				echo " $sec/$delay s"

				status=0
			else
				echo error: subtlehash: load_wait_bar: invalid width parameter 1>&2
			fi
		else
			echo error: subtlehash: load_wait_bar: invalid delay parameter 1>&2
		fi
	else
		echo error: subtlehash: load_wait_bar: need precisely 2 parameters 1>&2
	fi

	return $status
}

# Securely hashes passphrase, copies the hash to clipboard for a given delay and then wipes clipboard
subtlehash ()
{
	local status passline hashed hashAlg delay
	status=1

	# Tests number of parameters
	if (( $# == 2 )); then
		hashAlg="$1"
		delay="$2"
		echo -n "passphrase: "

		# Hashes passphrase
		if hashed="$(while read -s passline; do echo "$passline"; done | "$hashAlg")"; then

			# Copies passphrase to clipboard
			echo -n "${hashed%% *}" | xclip -selection clipboard
			echo
			echo passphrase copied !
			load_wait_bar $delay 50

			# Wipes passphrase from clipboard
			for i in {1..34}; do
				 passline=$RANDOM
				 hashed=`$hashAlg <<< $passline`
				 echo -n "${hashed%% *}" | xclip -selection clipboard
				 echo "[round $i/34] $passline --> $hashed"
			done
			echo passphrase erased !

			status=0
		fi
	else 
		echo 'subtlehash: usage: subtlehash <hashAlg> <delay>' 1>&2
	fi
	return $status
}
